# JavaScript - NodeJs console application

Project was built using nodejs and JavaScript.

This is a console application which will display 3 different options the user can chose from.
1. Read package.json
2. Display OS info
3. Start HTTP server

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

You can start by downloading the project in a .zip file, or cloning the project using the terminal.

### Running the project

Once the project has been downloaded, open a new terminal window and navigate to the project folder.
Once inside the folder, run the command npm install, which will install all required node modules.
After the npm install is finished, run the command npm start, to run the application.

## Maintainers 

[Robin Burø (@Robbur)](https://gitlab.com/Robbur)

## License

---
Copyright 2020, Robin Burø ([@Robbur](https://gitlab.com/Robbur))