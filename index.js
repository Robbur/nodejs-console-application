const readline = require('readline');
const os = require('os');
const fs = require('fs');
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const startApplication = () => {

  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  //Displays different choices
  console.log("Choose an option:");
  console.log("1. Read package.json");
  console.log("2. Display OS info");
  console.log("3. Start HTTP server");

  // Options run different functions based on input
  rl.question('Type a number: ', (option) => {
    switch (option) {
      case '1':
        console.log('Reading package.json file...');
        readPackageJSON()
        break;
      case '2':
        console.log('Getting OS info...');
        displayOsInformation()
        break;
      case '3':
        console.log('Starting HTTP server...');
        rl.close()
        startHTTPServer()
        break;
      default:
        console.log('\n\nInvalid option chosen.. please pick 1, 2 or 3....\n');
        console.log("\nStarting application again....\n");
        rl.close()
        startApplication()
    }
    rl.close();
  });

  // Read package.json file and write content to command line
  const readPackageJSON = () => {
    let rawdata = fs.readFileSync('package.json');
    let writeData = JSON.stringify(JSON.parse(rawdata), null, 2);
    console.log(writeData);
  }

  // Display OS information
  const displayOsInformation = () => {
    console.log('SYSTEM MEMORY: ' + (Math.floor((os.totalmem() / 1e9) * 100) / 100) + " GB");
    console.log('FREE MEMORY: ' + (Math.floor((os.freemem() / 1e9) * 100) / 100) + " GB");
    console.log('CPU CORES: ' + os.cpus().length);
    console.log('ARCH: ' + os.arch());
    console.log('PLATFORM: ' + os.platform());
    console.log('RELEASE: ' + os.release());
    console.log('USER: ' + os.userInfo().username);
  }

  // Start HTTP Server
  const startHTTPServer = () => {
    const server = http.createServer((req, res) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Hello World!');
    }).listen(port, hostname, () => {
      console.log(`listening on port ${port}...`);
    });
  }
}
startApplication()
